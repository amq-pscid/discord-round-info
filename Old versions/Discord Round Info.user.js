// ==UserScript==
// @name         Discord Round Info
// @version      1.5.3
// @description  Envia informações do round no AMQ para o Discord.
// @author       PsCid
// @match        https://animemusicquiz.com/*
// @grant        none
// @updateURL    https://gitlab.com/amq-pscid/discord-round-info/-/raw/master/Discord%20Round%20Info.user.js
// ==/UserScript==


// --------------------------------
// SHIFT + D = LIGA E DESLIGA
//
// TROCAR PARA O WEBHOOK DESEJADO (discordRoundWebhookUrl =  Envia info sobre a música do round)
var discordRoundWebhookUrl = "MUDAR"
// --------------------------------


if (document.getElementById("startPage")) return;
$(document.documentElement).keydown(function (event) {
    if (event.which === 68 && event.shiftKey === true) { // SHIFT + D
        toggleRoundInfo();
    }
});

var sendInfo = false;
function toggleRoundInfo() {
    if (sendInfo) {
        sendInfo = false;
        gameChat.systemMessage("Discord Webhook desligado.");
    } else {
        sendInfo = true;
        gameChat.systemMessage("Discord Webhook ligado para informações do round.");
    }
}

async function fetchWithTimeout(resource, options = {}) {
    const { timeout = 6000 } = options;

    const controller = new AbortController();
    const id = setTimeout(() => controller.abort(), timeout);

    const response = await fetch(resource, {
        ...options,
        signal: controller.signal
    });
    clearTimeout(id);

    return response;
}

async function fetchAnimeImage(anime) {
    try {
        const response = await fetchWithTimeout("https://api.jikan.moe/v3/anime/" + anime.malId + "/pictures", {
            timeout: 6000
        });
        const data = await response.json();
        return sendRoundWebhook(anime, data.pictures[0].small);
    } catch (error) {
        console.log(error.name === 'AbortError');
        return sendRoundWebhook(anime, 'https://cdn.discordapp.com/icons/386089398975856641/a_279f44d93d85cf091ebc3cbd738c3549.png');
    }
}

function sendRoundWebhook(anime, animeImage) {
    let guessesTextValue;
    if (quiz.gameMode == "Solo") {
        guessesTextValue = "Solo Game";
    } else {
        let guesses = anime.players.filter((tmpPlayer) => tmpPlayer.correct === true);
        guessesTextValue = guesses.length + "/" + anime.activePlayers + " (" + parseFloat((guesses.length/anime.activePlayers*100).toFixed(2)) + "%)";
        if (guesses.length <= 5 && guesses.length > 0) {
            var correctPlayers = "";
            for (var i = 0; i < anime.players.length; ++i) {
                if (anime.players[i].correct) {
                    correctPlayers = correctPlayers + anime.players[i].name + ", ";
                }
            }
            guessesTextValue = guessesTextValue + "\n{" + correctPlayers.substring(0, correctPlayers.length - 2) + "}";
        }
    }
    var fields = [
        {
            name: 'Inglês',
            value: anime.anime.english,
            inline: true,
        },
        {
            name: 'Romaji',
            value: anime.anime.romaji,
            inline: true,
        },
        {
            name: 'Música',
            value: anime.name + "\n\("+formatSamplePoint(anime.startSample, anime.videoLength)+"\)",
            inline: true,
        },
        {
            name: 'Tipo',
            value: anime.animeType,
            inline: true,
        },
        {
            name: 'Tema',
            value: anime.type,
            inline: true,
        },
        {
            name: 'Dificuldade',
            value: anime.difficulty,
            inline: true,
        },
        {
            name: 'Acertos',
            value: guessesTextValue,
            inline: true,
        },
        {
            name: 'Ano',
            value: anime.vintage,
            inline: true,
        },
        {
            name: 'Artista',
            value: anime.artist,
            inline: true,
        },
        {
            name: 'Links',
            value: anime.url,
            inline: false,
        },
    ];

    var botName;
    var roundTitle = "Round " + anime.songNumber + "/" + anime.totalSongs;
    if (quiz.gameMode == "Ranked") {
        botName = "AMQ Ranked " + document.getElementById("mpRankedTimer").getElementsByTagName("h3")[0].innerText
            + " - " + new Date().toLocaleDateString('pt-PT');
        let guesses = anime.players.filter((tmpPlayer) => tmpPlayer.correct === true);
        if (guesses.length == 1) {
           roundTitle = roundTitle + " - Solo Point!";
        }
    } else {
        var lobbyDiff;
        if (lobby.settings.songDifficulity.advancedOn) {
            lobbyDiff = lobby.settings.songDifficulity.advancedValue[0] + "-" + lobby.settings.songDifficulity.advancedValue[1];
        } else {
            if (lobby.settings.songDifficulity.standardValue.easy) {
                lobbyDiff = "Easy"
            }
            if (lobby.settings.songDifficulity.standardValue.medium) {
                lobbyDiff = lobbyDiff + ", Medium"
            }
            if (lobby.settings.songDifficulity.standardValue.hard) {
                lobbyDiff = lobbyDiff + ", Hard"
            }
        }
        botName = "AMQ " + quiz.gameMode + " - " + lobbyDiff;
    }
    webHookTemplate(discordRoundWebhookUrl, animeImage, botName, roundTitle, fields);
}

function webHookTemplate(discordWebhookUrl, animeImage, botName, type, fields) {
    if (discordWebhookUrl == "MUDAR") {
        gameChat.systemMessage("Discord Webhook não está configurado!");
    } else {
        fetch(
            discordWebhookUrl,
            {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    username: botName,
                    avatar_url:
                    'https://cdn.discordapp.com/icons/386089398975856641/a_279f44d93d85cf091ebc3cbd738c3549.png',
                    embeds: [
                        {
                            color: 15418782,
                            title: type,
                            fields: fields,
                            thumbnail: {
                                url: animeImage,
                            },
                            footer: {
                                text: 'AMQ',
                                icon_url:
                                'https://cdn.discordapp.com/icons/386089398975856641/a_279f44d93d85cf091ebc3cbd738c3549.png',
                            },
                        },
                    ],
                }),
            }
        );
    }
}

// Setup
let loadInterval = setInterval(() => {
    if (document.getElementById("loadingScreen").classList.contains("hidden")) {
        setup();
        clearInterval(loadInterval);
    }
}, 500);

function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function formatSamplePoint(start, length) {
    if (isNaN(start) || isNaN(length)) {
        return "Video not loaded";
    }
    let startPoint = Math.floor(start / 60) + ":" + (start % 60 < 10 ? "0" + (start % 60) : start % 60);
    let videoLength = Math.round(length);
    let totalLength = Math.floor(videoLength / 60) + ":" + (videoLength % 60 < 10 ? "0" + (videoLength % 60) : videoLength % 60);
    return startPoint + "/" + totalLength;
}


async function handleRoundInfoListener(result) {
    await delay(100);
    if (sendInfo) {
        var links;
        if (result.songInfo.urlMap.catbox.hasOwnProperty("0")) {
            links = "MAL: https://myanimelist.net/anime/" + result.songInfo.siteIds.malId;
        };
        if (result.songInfo.urlMap.catbox.hasOwnProperty("0")) {
            links = links + "\nmp3: " + result.songInfo.urlMap.catbox["0"];
        };
        if (result.songInfo.urlMap.catbox.hasOwnProperty("480")) {
            links = links + "\n480p: " + result.songInfo.urlMap.catbox["480"];
        };
        if (result.songInfo.urlMap.catbox.hasOwnProperty("720")) {
            links = links + "\n720p: " + result.songInfo.urlMap.catbox["720"];
        }
        let newSong = {
            name: result.songInfo.songName,
            anime: result.songInfo.animeNames,
            artist: result.songInfo.artist,
            songNumber: parseInt($("#qpCurrentSongCount").text()),
            totalSongs: parseInt($("#qpTotalSongCount").text()),
            difficulty: result.songInfo.animeDifficulty + "%",
            vintage: result.songInfo.vintage,
            animeType: result.songInfo.animeType,
            malId: result.songInfo.siteIds.malId,
            activePlayers: Object.values(quiz.players).filter(player => player.avatarSlot._disabled === false).length,
            type: result.songInfo.type === 3 ? "Insert Song" : (result.songInfo.type === 2 ? "Ending " + result.songInfo.typeNumber : "Opening " + result.songInfo.typeNumber),
            url: links,
            startSample: quizVideoController.moePlayers[quizVideoController.currentMoePlayerId].startPoint,
            videoLength: parseFloat(quizVideoController.moePlayers[quizVideoController.currentMoePlayerId].$player.find("video")[0].duration.toFixed(2)),
            players: Object.values(result.players)
            .map((tmpPlayer) => {
                let tmpObj = {
                    name: quiz.players[tmpPlayer.gamePlayerId]._name,
                    correct: tmpPlayer.correct,
                };
                return tmpObj;
            })
        };
        fetchAnimeImage(newSong);
    }
}

var playerRank;
var playerScore;
var playerName;
function setup() {
    let resultsListener = new Listener("answer results", (result) => {
        handleRoundInfoListener(result);
    });

    let quizOverListener = new Listener("quiz end result", (results) => {
        sendInfo = false;
    });

    let quizReturnToLobbyListener = new Listener("return lobby vote result", (payload) => {
        if (payload.passed) {
            sendInfo = false;
        }
    });

    let quizLeaveListener = new Listener("New Rooms", (rooms) => {
        sendInfo = false;
    });

    resultsListener.bindListener();
    quizOverListener.bindListener();
    quizLeaveListener.bindListener();
    quizReturnToLobbyListener.bindListener();
}